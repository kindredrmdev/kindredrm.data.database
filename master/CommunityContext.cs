﻿using System.Data.Common;
using System.Data.Entity;
using kindredrm.crosscutting.configuration;
using kindredrm.data.models.master;

namespace kindredrm.data.database.master
{
    public class CommunityContext : DbContext
    {
        static CommunityContext()
        {
            System.Data.Entity.Database.SetInitializer<CommunityContext>(null);
        }

        public CommunityContext(DbConnection connection, bool disableLazyLoading = false) : base(connection, false)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public CommunityContext(bool disableLazyLoading = false)
            : base(AppConfig.GetConnectionStringFor(Constants.ConnectionString.Master))
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public DbSet<CommunityModel> Communities { get; set; }
        public DbSet<SupplierModel> Suppliers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CommunityModelMap());
            modelBuilder.Entity<CommunityModel>()
                .HasMany(c => c.Suppliers)
                .WithMany(s => s.Communities)
                .Map(m =>
                {
                    m.MapLeftKey("CommunityID");
                    m.MapRightKey("SupplierID");
                    m.ToTable("CommunitySupplier");
                });

            modelBuilder.Configurations.Add(new SupplierModelMap());
            modelBuilder.Entity<SupplierModel>()
                .HasMany(s => s.Communities)
                .WithMany(c => c.Suppliers)
                .Map(m =>
                {
                    m.MapLeftKey("SupplierID");
                    m.MapRightKey("CommunityID");
                    m.ToTable("CommunitySupplier");
                });
        }
    }
}