﻿using System.Data.Common;
using System.Data.Entity;
using kindredrm.crosscutting.configuration;
using kindredrm.data.models.master;

namespace kindredrm.data.database.master
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbConnection connection, bool disableLazyLoading = false) : base(connection,
            disableLazyLoading)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public ProductContext(bool disableLazyLoading = false) : base(
            AppConfig.GetConnectionStringFor(Constants.ConnectionString.Master))
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public DbSet<ProductImageModel> ProductImages { get; set; }
        public DbSet<ProductModel> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductImageMap());
            modelBuilder.Configurations.Add(new ProductMap());
        }
    }
}