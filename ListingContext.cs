﻿using System.Data.Common;
using System.Data.Entity;
using kindredrm.data.models.communities;
using kindredrm.data.models.Communities.listings;

namespace kindredrm.data.database
{
    public class ListingContext : DbContext
    {
        //static ListingContext()
        //{
        //    System.Data.Entity.Database.SetInitializer<ListingContext>(null);
        //}

        public ListingContext(DbConnection connection, bool disableLazyLoading = false) : base(connection,
            disableLazyLoading)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public ListingContext(string connectionString, bool disableLazyLoading = false) : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public DbSet<CategoryModel> Categories { get; set; }
        public DbSet<ProductCategoryModel> ProductCategories { get; set; }
        public DbSet<ProductModel> Products { get; set; }
        public DbSet<BrandModel> Brands { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CategoryModelMap());
            modelBuilder.Configurations.Add(new ProductCategoryModelMap());
            modelBuilder.Configurations.Add(new ProductModelMap());
            modelBuilder.Configurations.Add(new SupplierModelMap());
        }
    }
}