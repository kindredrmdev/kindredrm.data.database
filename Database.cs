﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;

namespace kindredrm.data.database
{
    [Obsolete("Use kindredrm.data.database.BaseContext")]
    public abstract class Database : DbContext
    {
        protected DbContext Context { get; set; }

        /// <summary>
        ///     Updates the given fields for the dbcontext.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        protected virtual bool Update<T>(T entity, params Expression<Func<T, object>>[] properties) where T : class
        {
            using (Context)
            {
                Context.Set<T>().Attach(entity);
                foreach (var property in properties)
                {
                    var expression = property.Body is MemberExpression
                        ? (MemberExpression) property.Body
                        : (MemberExpression) ((UnaryExpression) property.Body).Operand;
                    Context.Entry(entity).Property(expression.Member.Name).IsModified = true;
                }
                return Context.SaveChanges() > 0;
            }
        }

        /// <summary>
        ///     Updates the given fields for the dbcontext.
        ///     Will not save or dispose of the context.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="context"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        protected virtual void Update<T>(T entity, DbContext context, List<Expression<Func<T, object>>> properties)
            where T : class
        {
            //TODO: REFACTOR THIS TO RECEIVE A COLLECTION OF ITENS FOR UPDATE
            context.Set<T>().Attach(entity);
            foreach (var property in properties)
            {
                var expression = property.Body is MemberExpression
                    ? (MemberExpression) property.Body
                    : (MemberExpression) ((UnaryExpression) property.Body).Operand;
                context.Entry(entity).Property(expression.Member.Name).IsModified = true;
            }
        }
    }
}