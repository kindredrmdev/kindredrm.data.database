﻿using System.Data.Common;
using System.Data.Entity;
using kindredrm.crosscutting.configuration;
using kindredrm.data.models.master.users;

namespace kindredrm.data.database
{
    public class UserContext : DbContext
    {
        static UserContext()
        {
            System.Data.Entity.Database.SetInitializer<UserContext>(null);
        }

        public UserContext(DbConnection connection, bool disableLazyLoading = false) : base(connection, false)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public UserContext(bool disableLazyLoading = false) : base(
            AppConfig.GetConnectionStringFor(Constants.ConnectionString.Master))
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<CommunityUser> CommunityUsers { get; set; }
        public DbSet<SupplierUser> SupplierUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserAccountMap());
            modelBuilder.Configurations.Add(new CommunityUserMap());
            modelBuilder.Configurations.Add(new SupplierUserMap());
        }
    }
}