﻿using System.Data.Common;
using System.Data.Entity;
using kindredrm.data.models.communities;

namespace kindredrm.data.database.community
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbConnection connection, bool disableLazyLoading = false) : base(connection,
            disableLazyLoading)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public ProductContext(string connectionString, bool disableLazyLoading = false) : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = !disableLazyLoading;
        }

        public DbSet<ProductImageModel> ProductImages { get; set; }
        public DbSet<ProductModel> Products { get; set; }
        public DbSet<ProductCategoryModel> ProductCategories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductImageModelMap());
            modelBuilder.Configurations.Add(new ProductModelMap());
            modelBuilder.Configurations.Add(new ProductCategoryModelMap());
        }
    }
}